<?php

namespace AppBundle\Extension\Twig;


class CheckerTypeExtension extends \Twig_Extension {


    public function isArray($var):bool{
        return is_array($var);
    }

    public function isDate($var):bool{
        return $var instanceof \Datetime;
    }

    public function getFunctions() {
        return [
            new \Twig_SimpleFunction('is_array', [$this, 'isArray']),
            new \Twig_SimpleFunction('is_date', [$this, 'isDate'])
        ];
    }

}
