<?php

namespace AppBundle\Subscriber;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Workflow\Event\Event;

class ValidateAlbumSubscriber implements EventSubscriberInterface {

    public function __construct(LoggerInterface $logger, SessionInterface $session){
        $this->logger = $logger;
        $this->session = $session;
    }

    public function transitionPublish(Event $event){
        $this->session->getFlashBag()->add('succes', 'Album '. $event->getSubject()->getTitle() . ' published');
    }

    public static function getSubscribedEvents(){
        return array(
            'workflow.validate_album.transition.publish' => array('transitionPublish')
        );
    }
}
