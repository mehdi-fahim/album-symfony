<?php

namespace AppBundle\Security\Voter;

use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;

class UserVoter extends Voter
{
    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager){
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, array('viewer', 'editor', 'manage'))) {
            return false;
        }

        // only vote on User objects inside this voter
        if (!$subject instanceof User) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        switch ($attribute) {
            case 'viewer':
                return $this->canView($subject, $user, $token);
            case 'editor':
                return $this->canEdit($subject, $user, $token);
            case 'manage':
                return $this->canDelete($subject, $user, $token);
        }

        throw new \LogicException('This code should not be reached!');
    }


    private function canView(User $subject, User $user, $token)
    {
        // if they can edit, they can view
        if ($this->canEdit($subject, $user, $token)) {
            return true;
        }

        return $this->decisionManager->decide($token, ['ROLE_VIEWER_USER']);
    }


    private function canEdit(User $subject, User $user, $token)
    {
        // if they can create, they can edit
        if ($this->canDelete($subject, $user, $token)) {
            return true;
        }

        if($this->decisionManager->decide($token, ['ROLE_EDITOR_USER'])){
            return true;
        }

        return $subject->getId() === $user->getId();
    }


    private function canDelete(User $subject, User $user, $token)
    {
        return $this->decisionManager->decide($token, ['ROLE_MANAGE_USER']);
    }
}