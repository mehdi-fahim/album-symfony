<?php

namespace AppBundle\Controller;

use AppBundle\Form\UserType;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


class SecurityController extends Controller
{
    /**
     * @Route("/login", name="user_login")
     */
    public function loginAction(Request $request, AuthenticationUtils $authenticationUtils)
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastMail = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', array(
            'mail'  => $lastMail,
            'error' => $error,
        ));
    }


    /**
     * @Route("/logout", name="user_logout")
     */
    public function logoutAction()
    {
        return $this->redirectToRoute('homepage');
    }


    /**
     * @Route("/register", name="user_registration")
     */
    public function registerAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        // 1) build the form
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        // get and assign role 'ROLE_MEMBER'
        $entityManager  = $this->getDoctrine()->getManager();
        $groupMember    = $entityManager->getRepository('AppBundle:Group')->findByRole('ROLE_MEMBER');
        if(!empty($groupMember)){
            $user->addGroup($groupMember[0]);
        }

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            // 3) Encode the password (you could also do this via Doctrine listener)
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            // 4) save the User!
            $entityManager->persist($user);

            $entityManager->flush();

            return $this->redirectToRoute('user_login');
        }

        return $this->render('security/register.html.twig',
            array('form' => $form->createView())
        );
    }
}