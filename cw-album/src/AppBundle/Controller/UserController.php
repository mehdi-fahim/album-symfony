<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * User controller.
 *
 * @Route("user")
 */
class UserController extends Controller
{
    /**
     * Lists all user entities.
     *
     * @Security("has_role('ROLE_EDITOR_USER')")
     * @Route("/", name="user_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $paramUsers = $em->getRepository('AppBundle:User')->findAll();

        return $this->render('user/index.html.twig', array(
            'users' => $paramUsers,
        ));
    }

    /**
     * Creates a new user entity.
     *
     * @Security("has_role('ROLE_AUTHOR_USER')")
     * @Route("/new", name="user_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $paramUser = new User();
        $form = $this->createForm('AppBundle\Form\UserType', $paramUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $password = $passwordEncoder->encodePassword($paramUser, $paramUser->getPlainPassword());
            $paramUser->setPassword($password);
            $em->persist($paramUser);

            $em->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/new-edit.html.twig', array(
            'user' => $paramUser,
            'form' => $form->createView(),
            'form_text' => ['title'=>'User Create', 'btn'=>'Create']
        ));
    }

    /**
     * Finds and displays a user entity.
     *
     * @Security("is_granted('viewer', paramUser)")
     * @Route("/{id}", name="user_show")
     * @Method("GET")
     */
    public function showAction(User $paramUser)
    {
        return $this->render('user/show.html.twig', array(
            'user' => $paramUser,
        ));
    }

    /**
     * Displays a form to edit an existing user entity.
     *
     * @Security("is_granted('editor', paramUser)")
     * @Route("/{id}/edit", name="user_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, User $paramUser)
    {
        $form = $this->createForm('AppBundle\Form\UserType', $paramUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('user_edit', array('id' => $paramUser->getId()));
        }

        return $this->render('user/new-edit.html.twig', array(
            'user' => $paramUser,
            'form' => $form->createView(),
            'form_text' => ['title'=>'User Update', 'btn'=>'Update'],
        ));
    }

    /**
     * Deletes a user entity.
     *
     * @Security("has_role('ROLE_MANAGE_ALBUM')")
     * @Route("/{id}/delete", name="user_delete")
     * @Method({"DELETE","GET"})
     */
    public function deleteAction(Request $request, User $paramUser)
    {
        $form = $this->createDeleteForm($paramUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($paramUser);
            $em->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/delete.html.twig', array(
            'user' => $paramUser,
            'form' => $form->createView()
        ));
    }

    /**
     * Creates a form to delete a user entity.
     *
     * @param User $paramUser The user entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(User $paramUser)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_delete', array('id' => $paramUser->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
