<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Album;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * Album controller.
 *
 * @Route("album")
 */
class AlbumController extends Controller
{
    /**
     * Lists all album entities.
     *
     * @Security("has_role('ROLE_EDITOR_ALBUM')")
     * @Route("/", name="album_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $albums = $em->getRepository('AppBundle:Album')->findAll();

        return $this->render('album/index.html.twig', array(
            'albums' => $albums,
        ));
    }

    /**
     * Creates a new album entity.
     *
     * @Security("has_role('ROLE_AUTHOR_ALBUM')")
     * @Route("/new", name="album_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $album = new Album();
        $form = $this->createForm('AppBundle\Form\AlbumType', $album);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($album);
            $em->flush();

            return $this->redirectToRoute('album_index');
        }

        return $this->render('album/new.html.twig', array(
            'album' => $album,
            'form' => $form->createView(),
            'form_text' => ['title'=>'Album Create', 'btn'=>'Create']
        ));
    }

    /**
     * Finds and displays a album entity.
     *
     * @Route("/{id}", name="album_show")
     * @Method("GET")
     */
    public function showAction(Album $album)
    {
        return $this->render('album/show.html.twig', array(
            'album' => $album,
        ));
    }

    /**
     * Displays a form to edit an existing album entity.
     *
     * @Security("has_role('ROLE_EDITOR_ALBUM')")
     * @Route("/{id}/edit", name="album_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Album $album)
    {
        $form = $this->createForm('AppBundle\Form\AlbumType', $album);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $workflow = $this->container->get('state_machine.validate_album');
            if($workflow->can($album, 'update')){
                $workflow->apply($album, 'update');
            }

            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('album_edit', array('id' => $album->getId()));
        }

        return $this->render('album/edit.html.twig', array(
            'album' => $album,
            'form' => $form->createView(),
            'album' => $album,
            'form_text' => ['title'=>'Album Update', 'btn'=>'Update'],
        ));
    }

    /**
     * Deletes a album entity.
     *
     * @Security("has_role('ROLE_MANAGE_ALBUM')")
     * @Route("/{id}/delete", name="album_delete")
     * @Method({"DELETE","GET"})
     */
    public function deleteAction(Request $request, Album $album)
    {
        $form = $this->createDeleteForm($album);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($album);
            $em->flush();

            return $this->redirectToRoute('album_index');
        }

        return $this->render('album/delete.html.twig', array(
            'album' => $album,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/{id}/state/{transition}", name="album_transition")
     * @Method("GET")
     */
    public function stateAction(Album $album, string $transition){

        $workflow = $this->container->get('state_machine.validate_album');

        if($workflow->can($album, $transition)){
            $workflow->apply($album, $transition);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($album);
        $em->flush();

        return $this->redirectToRoute('album_edit', [
            'id' => $album->getId()
        ]);
    }



    /**
     * Creates a form to delete a album entity.
     *
     * @param Album $album The album entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Album $album)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('album_delete', array('id' => $album->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
